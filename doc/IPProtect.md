Smart Contract "IPProtect"
==============================
### How to use smart-contracts functions:
 
You can use a smart-contract by the name of its object(example: `IPProtect`)
 
For call/send transaction to smart-contract you should use `async/await`, but will be work `then`.
 
Use `call()` for methods, that do not require transaction fees

Use `send({[from] [, value] [, gasLimit]})` for methods, that entail data changes (monipulation with tokens, providing access)
USe `web3.utils` for some convert, how in exemples. 
 Exemples for use smart-contract `IPProtect`:

```javascript
  //Start Green Light Company
  const purchasingPrice = '350 USD';
  const paymentTerms = '50/50';
  const leadTime = '3';
  const deliveryTerms = 'to door delivery';
  const termsAndConditions = '';
  const timeEvent = '0';
  let result = await IPProtect.methods.startEvent(
      web3.utils.fromAscii(purchasingPrice),
      web3.utils.fromAscii(paymentTerms),
      web3.utils.fromAscii(leadTime),
      web3.utils.fromAscii(deliveryTerms),
      termsAndConditions,
      timeEvent,
    )
    .send({
      from:admin
  });
    
  //Receive the brand address of product
  let result = await IPProtect.methods.getBrandAddress().call();
  res.json({ "Address brand": result });
    
  //Receive the product address for this smart contract
  let result = await IPProtect.methods.getProductAddress().call();
  res.json({ "Address product": result });
    
  //Receive the date of product creation
  let result = await IPProtect.methods.getDateCreation().call();
  res.json({ "Date of Creation IP": result });
    
  //Receive the date of start Green Light Company
  let result = await IPProtect.methods.getDateEvent().call();
  res.json({ "Date of start Green Light Company": result });
  
  //Receive details of product (product id, sangrove id)
  let result = await IPProtect.methods.getDetails().call();
  res.json({ "Details of product": result });
  
  //Receive images of product (images hash: first, second, third)
  let result = await IPProtect.methods.getImages().call();
  res.json({ "Images of product": result });

  //Get price of product
  let result = await IPProtect.methods.getPurchasingPrice().call();
  res.json({ "Price of product": result });

  //Get payment terms of product
  let result = await IPProtect.methods.getPaymentTerms().call();
  res.json({ "Payment terms of product": result });

  //Get lead time of product
  let result = await IPProtect.methods.getLeadTime().call();
  res.json({ "Lead time of product": result });

  //Get delivery terms of product
  let result = await IPProtect.methods.getDeliveryTerms().call();
  res.json({ "Delivery terms of product": result });

  //Get link to page "Terms and Conditions"
  let result = await IPProtect.methods.getTermsAndConditions().call();
  res.json({ "Terms and Conditions of produc": result });
  
  //Add retailer when buy product
  const addressRetailer = "0x9f90097f9a0c6f93CdE097d4b735a78595899AFc";
  const amount = 500; 
  await IPProtect.methods.sell(addressRetailer, amount).call();

  //Add retailer, agent and add quantity of product sold
  const addressRetailer = "0x9f90097f9a0c6f93CdE097d4b735a78595899AFc";
  const addressAgent = account[3];
  const amount = 500;
  await IPProtect.methods.sellWithAgent(addressRetailer, addressAgent, amount).call();
    
  //Get quantity of product sold
  let result = await IPProtect.methods.getSold().call();
  res.json({ "Quantuty of product sold": result });
    
  //Get amount particular retailer bought
  const addressRetailer = "0x9f90097f9a0c6f93CdE097d4b735a78595899AFc";
  let result = await IPProtect.methods.getRetailerBuy(addressRetailer).call();
  res.json({ "Retailer bought": result });
    
  //Get the agent for the retailer
  const addressRetailer = "0x9f90097f9a0c6f93CdE097d4b735a78595899AFc";
  let result = await IPProtect.methods.getRetailerAgent(addressRetailer).call();
  res.json({ "This Seller's Agent": result });

  //Set link to page "Terms and Conditions
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const link = "https://sangrove.com/termsAndConditions";
  await sangrovetoken.methods.setTermsAndConditions(link).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change brand address
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const brand = "0x7bf27dFB50B08BB1Ae154523f9ad2ab09099acb6";
  await sangrovetoken.methods.setBrandAddress(brand).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change product address
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const product = "0x2ac27dFB50B08BB1Ae154523f9ad2ab090997bcd";
  await sangrovetoken.methods.setProductAddress(product).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change date creation of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const timestamp = 1579189733;
  await sangrovetoken.methods.setDateCreation(timestamp).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change date Event of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const timestamp = 1579189733;
  await sangrovetoken.methods.setDateEvent(timestamp).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change brand product id
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const productId = web3.utils.fromAscii('GSP7140/41');
  await sangrovetoken.methods.setBrandProductId(productId).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change sangrove product id
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const productId = 83;
  await sangrovetoken.methods.setSangroveProductId(productId).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change images for product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const newImage1 = '0x73616e67726f76652049702070726f74656374';
  const newimage2 = '0x69702070726f74656374';
  const newimage3 = '0x73616e67726f7665';
  await sangrovetoken.methods.setImages(newImage1, newimage2, newimage3).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change retailer amount of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const addressRetailer = "0x2ac27dFB50B08BB1Ae154523f9ad2ab090997bcd";
  await sangrovetoken.methods.setRetailerBuyAmount(addressRetailer, 500).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change agent for ratailer of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const addressRetailer = "0x2ac27dFB50B08BB1Ae154523f9ad2ab090997bcd";
  const addressAgent = "0xDBE7b5E913B3267AdC3ac801B298120D5573B43B";
  await sangrovetoken.methods.setRetailerBuyAmount(addressRetailer, addressAgent).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change purchasing price of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const price = web3.utils.fromAscii('250 USD');
  await sangrovetoken.methods.setPurchasingPrice(price).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change payment terms of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const terms = web3.utils.fromAscii('30/70');
  await sangrovetoken.methods.setPaymentTerms(terms).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set/change delivery terms of product
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const terms = web3.utils.fromAscii('EXW at regional warehouse');
  await sangrovetoken.methods.setDeliveryTerms(terms).send({
    from: admin
  });
  res.json({ success: result.status });

  //Set Hash of uploaded file by Admin with Terms & Conditions of Brand
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const TAC = 'https://sangrove.com/termsandconditions';
  await sangrovetoken.methods.setTermsAndConditions(TAC).send({
    from: admin
  });
  res.json({ success: result.status });
```