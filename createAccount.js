 //Model file for smart-contracts
  import env from 'dotenv';
  import Web3 from 'web3';
  import HDWalletProvider from 'truffle-hdwallet-provider';
  import * as tokenJSON from '../contracts/SangroveToken';
  import * as ipprotectJSON from '../contracts/IPProtect';
  import { config } from '../config';

  const provider = new HDWalletProvider(env.config().parsed.mnemonic, config.matic.netURL);
  const web3 = new Web3(provider);
