module.exports = {
    matic: {
        netURL:  `https://polygon-mumbai.infura.io/v3/421c197d06d14c4e8835ee46a62d23f9`//https://ropsten.infura.io/v3/4fa18aa461584151b5e1d8728b6df65e`
    },
    network_id: 80001,       // Ropsten's id
    gas: 55000000,        // Ropsten has a lower block limit than mainnet
    confirmations: 2,    // # of confs to wait between deployments. (default: 0)
    timeoutBlocks: 200,  // # of blocks before a deployment times out  (minimum/default: 50)
    skipDryRun: true     // Skip dry run before migrations? (default: false for public nets )
  }
