const assert = require('assert');
// eslint-disable-next-line no-undef
const Token = artifacts.require('SangroveToken');

// eslint-disable-next-line no-undef
contract('Test SangroveToken functions', async(account) => {
  const address1 = account[1];
  const address2 = account[2];
  const address3 = account[3];
  const address4 = account[4];
  describe('Check Token Information', async() => {
    let token;
    before(async() => {
      token = await Token.deployed();
    }, 'Deploy Token Smart-Contract');

    it('Receive token name', async() => {
      assert.equal(await token.name(), 'SANGROVE');
    });

    it('Receive token symbol', async() => {
      assert.equal(await token.symbol(), 'GRO');
    });

    it('Receive token decimals', async() => {
      assert.equal(await token.decimals(), 0);
    });

    it('Receive token total supply', async() => {
      assert.equal(await token.totalSupply(), 0);
    });

    it('Receive the token balance of another account', async() => {
      assert.equal(await token.balanceOf(address1), 0);
    });
    it('Function to mint tokens and approve they to spend by Sangrove', async() => {
      const recipient = address1;
      const admin = account[0];
      const amount = 5000;
      const result = await token.mintAndApprove(recipient, amount, {
        from: admin,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Send tokens to another account', async() => {
      const sender = address1;
      const recipient = address2;
      const owner = account[0];
      const amount = 5000;
      await token.transferFrom(sender, recipient, amount, {
        from: owner,
        value: 0,
        gas: 1000000,
      });
      assert.equal(await token.balanceOf(address1), 0);
      assert.equal(await token.balanceOf(address2), 5000);
    });
    it('Burns a specific amount of tokens from the target address and decrements allowance', async() => {
      const owner = address2;
      const admin = account[0];
      const amount = 3000;

      await token.burnFrom(owner, amount, {
        from: admin,
      });
      assert.equal(await token.balanceOf(address2), 2000);
    });
    describe('Scenario for Function mintAndApprove()', () => {
      const owner = address3;
      const recipient = address4;
      const admin = account[0];
      const amount = 1500;
      it('Function to mint tokens and approve they to spend by Sangrove', async() => {
        const result = await token.mintAndApprove(owner, amount, {
          from: admin,
        });
        assert.equal(result.receipt.status, true);
      });
      it('Function to transfer tokens from owner to recepiver by Sangrove', async() => {
        const result = await token.transferFrom(owner, recipient, amount, {
          from: admin,
          gas: 1000000,
        });
        assert.equal(result.receipt.status, true);
      });
      it('Function to get balance recipient tokens ', async() => {
        const result = await token.balanceOf(recipient);
        assert.equal(result, 1500);
      });
    });
  });
});
