Smart Contract "sangroveToken"
==============================
>The following describes the interface of the token smart contract
>This will allow the Sangrove platform to process tokens through the interface.

>This set of interfaces, contracts, and utilities are all related to the [ERC20 Token Standard](https://eips.ethereum.org/EIPS/eip-20).

There a few core contracts that implement the behavior specified in the EIP:

 - IERC20: the interface all ERC20 implementations should conform to
 
 - ERC20: the base implementation of the ERC20 interface
 
 - ERC20Detailed: includes the name, symbol and decimals optional standard extension to the base interface
 
 - ERC20Mintable: designation of addresses that can create token supply
 
 ----------
### How to use smart-contracts functions:
 
You can use a smart-contract by the name of its object(example: `sangroveToken`)
 
For call/send transaction to smart-contract you should use `async/await`, but will be work `then`.
 
Use `call()` for methods, that do not require transaction fees

Use `send({[from] [, value] [, gasLimit]})` for methods, that entail data changes (monipulation with tokens, providing access)

 Exemples for use smart-contract `sangroveToken`:

```javascript
  //Receive token name
  let result = await sangroveToken.methods.name().call();
	res.json({ "Name of Token": result });
    
  //Receive token symbol
  let result = await sangroveToken.methods.symbol().call();
	res.json({ "Symbol of Token": result });
    
  //Receive token decimals
  let result = await sangroveToken.methods.decimals().call();
	res.json({ "Decimals of Token": result });
    
  //Receive token total supply
  let result = await sangroveToken.methods.totalSupply().call();
	res.json({ "Total supply of Token": result });
    
  //Receive the token balance of another account
  let result = await sangroveToken.methods.balanceOf(req.params.address).call();
	res.json({ "Balance of Token": result });

  //Send tokens from sender account to receiver
  const sender = "0xDBE7b5E913B3267AdC3ac801B298120D5573B43B";
  const receiver = "0x9f90097f9a0c6f93CdE097d4b735a78595899AFc";
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const amount = 2000;
  await sangroveToken.methods.transferFrom(sender, receiver, amount).send({
    from: admin,
  });
  
  //Burns a specific amount of tokens from the target address and decrements allowance
  const owner = "0xDBE7b5E913B3267AdC3ac801B298120D5573B43B";
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const amount = 3000;
  await sangroveToken.methods.burnFrom(owner, amount).send({
    from: admin
  });
  
  //Function to mint tokens and approve they to spend by Sangrove
  const owner = account[3];
  const receiver = account[4];
  const admin = "0x8bB27dFB50B08BB1Ae154523f9ad2ab0909971c4";
  const amount = 1500;
  await sangroveToken.methods.mintAndApprove(owner, amount).send({
    from: admin
  });
```