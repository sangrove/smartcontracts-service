
// File: @openzeppelin/contracts/ownership/Ownable.sol

pragma solidity ^0.5.0;

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be aplied to your functions to restrict their use to
 * the owner.
 */
contract Ownable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(isOwner(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Returns true if the caller is the current owner.
     */
    function isOwner() public view returns (bool) {
        return msg.sender == _owner;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * > Note: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     */
    function _transferOwnership(address newOwner) internal {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

// File: contracts/IPProtect.sol

pragma solidity ^0.5.0;


contract IPProtect is Ownable {
    struct Details {
        bytes32 brandProductId;
        int64 systemProductId;
    }
    struct Images{
        bytes32 image1;
        bytes32 image2;
        bytes32 image3;
    }

    address brand;
    address product;
    bytes32 purchasingPrice;
    bytes32 paymentTerms;
    bytes32 leadTime;
    bytes32 deliveryTerms;
    uint timeCreated;
    uint timeEvent;
    string termsAndConditions;
    Details productDetails;
    Images productImages;

    mapping(address => uint) retailersBuyAmount;
    mapping(address => address) retailersAgent;

    constructor(
        address _brand,
        address _product,
        bytes32 _brandProductId,
        int64 _systemProductId,
        bytes32 _image1,
        bytes32 _image2,
        bytes32 _image3,
        uint _timeCreated
    )
    public {
        require(_brand != address(0), 'IP Protect: Invalid brand address');
        require(_product != address(0), 'IP Protect: Invalid brand address');
        require(_systemProductId >= 0, 'IP Protect: Invalid system Product Id value');
        brand = _brand;
        product = _product;
        productDetails = Details({brandProductId: _brandProductId, systemProductId: _systemProductId});
        productImages = Images({image1: _image1, image2: _image2, image3: _image3});
        if(_timeCreated == 0)
            timeCreated = now;
        else
            timeCreated = _timeCreated;
    }
    function startEvent(
        bytes32 _purchasingPrice,
        bytes32 _paymentTerms,
        bytes32 _leadTime,
        bytes32 _deliveryTerms,
        string memory _termsAndConditions,
        uint _timeEvent
    ) public returns(bool) {
        purchasingPrice = _purchasingPrice;
        paymentTerms = _paymentTerms;
        leadTime = _leadTime;
        deliveryTerms = _deliveryTerms;
        termsAndConditions = _termsAndConditions;
        if(_timeEvent == 0)
            timeEvent = now;
        else
            timeEvent = _timeEvent;
        return true;
    }

    function getBrandAddress() public view returns(address) {
        return brand;
    }
    function getProductAddress() public view returns(address) {
        return product;
    }
    function getDateCreation() public view returns(uint date) {
        return timeCreated;
    }
    function getDateEvent() public view returns(uint date) {
        return timeEvent;
    }
    function getDetails() public view returns(bytes32 brandProductId, int64 systemProductId) {
        return (
            productDetails.brandProductId,
            productDetails.systemProductId
        );
    }
    function getImages() public view returns(bytes32 image1, bytes32 image2, bytes32 image3) {
        return (
            productImages.image1,
            productImages.image2,
            productImages.image3
        );
    }
    function sell(address retailer, uint amount) public returns(bool success) {
        require(retailer != address(0), 'IP Protect: Invalid Retailer address');
        require(amount >= 0, 'IP Protect: Invalid amount');
        retailersBuyAmount[retailer] += amount;
        return true;
    }
    function sellWithAgent(address retailer, address agent, uint amount) public returns(bool success) {
        require(retailer != address(0), 'IP Protect: Invalid Retailer address');
        require(agent != address(0), 'IP Protect: Invalid Agent address');
        require(amount >= 0, 'IP Protect: Invalid amount');
        retailersBuyAmount[retailer] += amount;
        retailersAgent[retailer] = agent;
        return true;
    }
    function getRetailerBuy(address retailer) public view returns(uint amount) {
        require(retailer != address(0), 'IP Protect: Invalid Retailer address');
        return retailersBuyAmount[retailer];
    }
    function getRetailerAgent(address retailer) public view returns(address agent) {
        require(retailer != address(0), 'IP Protect: Invalid Retailer address');
        return retailersAgent[retailer];
    }
    function getPurchasingPrice() public view returns(bytes32 price) {
        return purchasingPrice;
    }
    function getPaymentTerms() public view returns(bytes32 terms) {
        return paymentTerms;
    }
    function getLeadTime() public view returns(bytes32 time) {
        return leadTime;
    }
    function getDeliveryTerms() public view returns(bytes32 terms) {
        return deliveryTerms;
    }
    function getTermsAndConditions() public view returns(string memory) {
        return termsAndConditions;
    }
    function setBrandAddress(address _brand) public onlyOwner returns(bool success) {
        require(_brand != address(0), 'IP Protect: Invalid brand address');
        brand = _brand;
        return true;
    }
    function setProductAddress(address _product) public onlyOwner returns(bool success) {
        require(_product != address(0), 'IP Protect: Invalid product address');
        product = _product;
        return true;
    }
    function setDateCreation(uint _timeCreated) public onlyOwner returns(bool success) {
        timeCreated = _timeCreated;
        return true;
    }
    function setDateEvent(uint _timeEvent) public onlyOwner returns(bool success) {
        timeEvent = _timeEvent;
        return true;
    }
    function setBrandProductId(bytes32 brandProductId) public onlyOwner returns(bool success) {
        productDetails.brandProductId = brandProductId;
        return true;
    }
    function setSystemProductId(int64 systemProductId) public onlyOwner returns(bool success) {
        require(systemProductId >= 0, 'IP Protect: Invalid System Product Id value');
        productDetails.systemProductId = systemProductId;
        return true;
    }
    function setImages(bytes32 image1, bytes32 image2, bytes32 image3) public onlyOwner returns(bool success) {
        productImages.image1 = image1;
        productImages.image2 = image2;
        productImages.image3 = image3;
        return true;
    }
    function setRetailerBuyAmount(address retailer, uint amount) public onlyOwner returns(bool success) {
        require(retailer != address(0), 'IP Protect: Invalid Retailer address');
        retailersBuyAmount[retailer] = amount;
        return true;
    }
    function setRetailerAgent(address retailer, address agent) public onlyOwner returns(bool success) {
        require(retailer != address(0), 'IP Protect: Invalid Retailer address');
        require(agent != address(0), 'IP Protect: Invalid Agent address');
        retailersAgent[retailer] = agent;
        return true;
    }
    function setPurchasingPrice(bytes32 _purchasingPrice) public onlyOwner returns(bool success) {
        purchasingPrice = _purchasingPrice;
        return true;
    }
    function setPaymentTerms(bytes32 _paymentTerms) public onlyOwner returns(bool success) {
        paymentTerms = _paymentTerms;
        return true;
    }
    function setLeadTime(bytes32 _leadTime) public onlyOwner returns(bool success) {
        leadTime = _leadTime;
        return true;
    }
    function setDeliveryTerms(bytes32 _deliveryTerms) public onlyOwner returns(bool success) {
        deliveryTerms = _deliveryTerms;
        return true;
    }
    function setTermsAndConditions(string memory _termsAndConditions) public onlyOwner returns(bool success){
        termsAndConditions = _termsAndConditions;
        return true;
    }
}
