pragma solidity ^0.5.0;

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';
import '@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol';
import '@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol';
import '@openzeppelin/contracts/ownership/Ownable.sol';

contract SangroveToken is ERC20, ERC20Detailed, ERC20Mintable, Ownable {
    constructor() ERC20Detailed("SANGROVE", "GRO", 0) public {
    }
    function transferFrom(address sender, address recipient, uint256 amount) public onlyOwner returns (bool success) {
        require(sender != address(0), 'Sangrove Token: Invalid Sender address');
        require(recipient != address(0), 'Sangrove Token: Invalid Retailer address');
        require(sender != address(this), "Sangrove Token: you inserted contract's address");
        require(recipient != address(this), "Sangrove Token: you inserted contract's address");
        require(amount >= 0, 'Sangrove Token: Invalid amount value');
        allowance(sender, msg.sender).sub(amount);
        _transfer(sender, recipient, amount);
        _approve(recipient, msg.sender, balanceOf(recipient));
        return true;
    }
    function burnFrom(address account, uint256 amount) public onlyOwner returns (bool success) {
        require(account != address(0), 'Sangrove Token: Invalid address');
        require(amount >= 0, 'Sangrove Token: Invalid amount value');
        _burnFrom(account, amount);
        return true;
    }
    function mintAndApprove(address to, uint256 amount) public onlyOwner returns (bool success) {
        require(address(this) != to, "Sangrove Token: you inserted contract's address");
        require(amount >= 0, 'Sangrove Token: Invalid amount value');
        _mint(to, amount);
        _approve(to, msg.sender, balanceOf(to));
        return true;
    }
}
