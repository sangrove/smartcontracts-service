const web3 = require('web3');
const assert = require('assert');
// eslint-disable-next-line no-undef
const IPProtect = artifacts.require('IPProtect');


// eslint-disable-next-line no-undef
contract('Test IP Protection functions', async(account) => {
  const owner = account[0];
  const addressBrand = '0x627306090abaB3A6e1400e9345bC60c78a8BEf57';
  const addressProduct = '0xf17f52151EbEF6C7334FAD080c5704D77216b732';
  const brandProductId = 'DPS11.01.5.1.3.3 - 03';
  const systemProductId = 75;
  const purchasingPrice = '350 USD';
  const paymentTerms = '50/50';
  const leadTime = '3';
  const deliveryTerms = 'to door delivery';
  const termsAndConditions = '';
  const image1 = '0xc642c686e44a00a6edf28a978930a2707cd01272349225c31476e8f836a8bdac';
  const image2 = '0x';
  const image3 = '0x';
  const timeEvent = '0';
  const timeCreated = 0;
  let ipp;
  before(async() => {
    ipp = await IPProtect.new(
      addressBrand,
      addressProduct,
      web3.utils.fromAscii(brandProductId),
      systemProductId,
      image1,
      image2,
      image3,
      timeCreated,
    );
  }, 'Deploy IP Protection');
  describe('Check IP Protection Information', async() => {
    it('Start Green Light Company', async() => {
      const result = await ipp.startEvent(
        web3.utils.fromAscii(purchasingPrice),
        web3.utils.fromAscii(paymentTerms),
        web3.utils.fromAscii(leadTime),
        web3.utils.fromAscii(deliveryTerms),
        termsAndConditions,
        timeEvent,
      );
      assert.equal(result.receipt.status, true);
    });

    it('Receive the brand address of product', async() => {
      const result = await ipp.getBrandAddress();
      assert.equal(result, addressBrand);
    });

    it('Receive the product address for this smart contract', async() => {
      const result = await ipp.getProductAddress();
      assert.equal(result, addressProduct);
    });

    it('Receive the date of product creation', async() => {
      const result = await ipp.getDateCreation();
      const resultDate = new Date(result * 1000).getDate();
      const currentDate = new Date().getDate();
      assert.equal(resultDate, currentDate);
    });

    it('Receive the date of start Green Light Company', async() => {
      const result = await ipp.getDateEvent();
      const resultDate = new Date(result * 1000).getDate();
      const currentDate = new Date().getDate();
      assert.equal(resultDate, currentDate);
    });

    it('Receive details of product', async() => {
      const result = await ipp.getDetails();
      const resProductId = result[0];
      const resSystemId = result[1];
      assert.equal(web3.utils.hexToString(resProductId), brandProductId);
      assert.equal(resSystemId, systemProductId);
    });

    it('Receive images of product', async() => {
      const result = await ipp.getImages();
      assert.equal(web3.utils.toBN(result[0]).toString(), web3.utils.toBN(image1).toString());
      assert.equal(web3.utils.toBN(result[1]).toString(), web3.utils.toBN(image2).toString());
      assert.equal(web3.utils.toBN(result[2]).toString(), web3.utils.toBN(image3).toString());
    });
    it('Get price of product', async() => {
      const result = await ipp.getPurchasingPrice();
      assert.equal(web3.utils.hexToString(result), purchasingPrice);
    });
    it('Get payment terms of product', async() => {
      const result = await ipp.getPaymentTerms();
      assert.equal(web3.utils.hexToString(result), paymentTerms);
    });
    it('Get lead time of product', async() => {
      const result = await ipp.getLeadTime();
      assert.equal(web3.utils.hexToString(result), leadTime);
    });
    it('Get delivery terms of product', async() => {
      const result = await ipp.getDeliveryTerms();
      assert.equal(web3.utils.hexToString(result), deliveryTerms);
    });
    it('Get terms and conditions of product', async() => {
      const result = await ipp.getTermsAndConditions();
      assert.equal(result, termsAndConditions);
    });
  });
  describe('Scenario for buy product with and without agent and check ballance()', async() => {
    it('Add retailer when buy product', async() => {
      const addressRetailer = account[2];
      const amount = 500;
      const result = await ipp.sell(addressRetailer, amount);
      assert.equal(result.receipt.status, true);
    });

    it('Add retailer, agent and add quantity of product sold', async() => {
      const addressRetailer = account[2];
      const addressAgent = account[3];
      const amount = 500;
      const result = await ipp.sellWithAgent(addressRetailer, addressAgent, amount);
      assert.equal(result.receipt.status, true);
    });

    it('Get amount particular retailer bought', async() => {
      const addressRetailer = account[2];
      const result = await ipp.getRetailerBuy(addressRetailer);
      assert.equal(result, 1000);
    });

    it('Get the agent for the retailer', async() => {
      const addressRetailer = account[2];
      const addressAgent = account[3];
      const result = await ipp.getRetailerAgent(addressRetailer);
      assert.equal(result, addressAgent);
    });
  });
  describe('Change IP Protect information', async() => {
    it('Set/change brand address', async() => {
      const brand = account[3];
      const result = await ipp.setBrandAddress(brand, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change product address', async() => {
      const product = account[2];
      const result = await ipp.setProductAddress(product, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change date creation of product', async() => {
      const timestamp = 1579189733;
      const result = await ipp.setDateCreation(timestamp, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change date Event of product', async() => {
      const timestamp = 1579190952;
      const result = await ipp.setDateEvent(timestamp, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change brand product id', async() => {
      const productId = web3.utils.fromAscii('GSP7140/41');
      const result = await ipp.setBrandProductId(productId, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change system product id', async() => {
      const productId = 83;
      const result = await ipp.setSystemProductId(productId, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change images for product', async() => {
      const newImage1 = '0x73616e67726f76652049702070726f74656374';
      const newimage2 = '0x69702070726f74656374';
      const newimage3 = '0x73616e67726f7665';
      const result = await ipp.setImages(newImage1, newimage2, newimage3, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change retailer amount of product', async() => {
      const addressRetailer = account[2];
      const result = await ipp.setRetailerBuyAmount(addressRetailer, 500, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change agent for ratailer of product', async() => {
      const addressRetailer = account[2];
      const addressAgent = account[4];
      const result = await ipp.setRetailerAgent(addressRetailer, addressAgent, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change purchasing price of product', async() => {
      const price = web3.utils.fromAscii('250 USD');
      const result = await ipp.setPurchasingPrice(price, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change payment terms of product', async() => {
      const terms = web3.utils.fromAscii('30/70');
      const result = await ipp.setPaymentTerms(terms, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set/change delivery terms of product', async() => {
      const terms = web3.utils.fromAscii('EXW at regional warehouse');
      const result = await ipp.setDeliveryTerms(terms, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
    it('Set terms and conditions of product', async() => {
      const TAC = 'https://system.com/termsandconditions';
      const result = await ipp.setTermsAndConditions(TAC, {
        from: owner,
      });
      assert.equal(result.receipt.status, true);
    });
  });
});
