Sangrove Smart Contracts
==============================

## Installation

### For local Server:

#### node & npm
To install and use node LTS version:

[Node JS(with npm)](https://nodejs.org/uk/)

#### truffle

To install:
`npm -g truffle`

### Environment desc

`$ truffle version`

>Truffle v5.1.4 (core: 5.1.4)

>Solidity v0.5.12 (solc-js)

>Node v12.13.0

>Web3.js v1.2.1

### To install modules:

`$ npm install`

### To run tests:
`$ npm test`

This command starts a docker based _ganache-cli_ instance and invokes tests via truffle framework

 ----------
 Interface of the ERC20 standard as defined in the EIP. Does not include the optional functions;

### Deploy to TestNet:

>1.Create project on [Infura](https://infura.io/dashboard)

>2.Create your wallet with 2+ ETH for transactions & Get mnemonic:

>>+ Install MetaMask from google chrome [webstore](https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn) 

>>+ Click on the favicon MetaMask on the top right of your browser

>>+ Click on `Get Started`, then on `Create a wallet`, `I agree`

>>+ Enter password for wallet on the New Account page and click on `Create`

>>+ On next page you can see Secret Backup Phrase, click to gray field and copy phrase, it is your `mnemonic`

>>+ Click on `Remind me later`

>>+ For get 2+ ETH on TestNet, on the top right select `Ropsten Test Network`

>>+ Click on the `Deposit` button, on new pop up click on the `Get Ether` button, from Test Faucet

>>+ Click on `request 1 ether from faucet`, on new window click on `Connect`

>>+ You get 1 eth after 1 minute about, to get another one click on `request 1 ether from faucet` 

>>+ For check, how much money you have, click on the favicon MetaMask on the top right of your browser and in new pop up you see balance

>3.In truffle-config.js

>* set your mnemonic(12 word)
>* set ropsten ENDPOINT from Infura (https://ropsten.infura.io/v3/<ProjectID>)
    
>4.Delete folder 'build'

>5.In Terminal your Project insert:

>* $ truffle compile
>* $ truffle deploy --network ropsten
>* **contract address you can get from console after successful deploy**

>6.File 'build/contracts/NameofContract.json' it's your deploy Contract!

### How to connect server to smart-contract after deployed:

>+ Installing the necessary modules:

>* `$ npm i --save truffle-hdwallet-provider`

>* `$ npm i --save web3`

```javascript
  //Model file for smart-contracts
  import env from 'dotenv';
  import Web3 from 'web3';
  import HDWalletProvider from 'truffle-hdwallet-provider';
  import * as tokenJSON from '../contracts/SangroveToken';
  import * as ipprotectJSON from '../contracts/IPProtect';
  import { config } from '../config';

  const provider = new HDWalletProvider(env.config().parsed.mnemonic, config.ropsten.netURL);
  const web3 = new Web3(provider);

  const {abi: abiToken} = tokenJSON;
  export const sangroveToken = new web3.eth.Contract(abiToken, config.ropsten.sangroveToken);

  const {abi: abiIPProtect} = ipprotectJSON;
  export const ipprotect = new web3.eth.Contract(abiIPProtect, config.ropsten.IPProtect);
```
### Smart-Contracts documentation:
[SangroveToken](./doc/SangroveToken.md)
    
[IP Protect](./doc/IPProtect.md)