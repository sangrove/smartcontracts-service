const web3 = require('web3');
// eslint-disable-next-line no-undef
const IPProtect = artifacts.require('./IPProtect.sol');
const addressBrand = '0x627306090abaB3A6e1400e9345bC60c78a8BEf57';
const addressProduct = '0xf17f52151EbEF6C7334FAD080c5704D77216b732';
const brandProductId = 'DPS11.01.5.1.3.3 - 03';
const systemProductId = 75;
const image1 = '0xc642c686e44a00a6edf28a978930a2707cd01272349225c31476e8f836a8bdac';
const image2 = '0x';
const image3 = '0x';
const timeCreated = 0;
module.exports = (deployer) => {
  deployer.deploy(
    IPProtect,
    addressBrand,
    addressProduct,
    web3.utils.fromAscii(brandProductId),
    systemProductId,
    image1,
    image2,
    image3,
    timeCreated,
  );
};
