import http from 'http';
import env from 'dotenv';
import Web3 from 'web3';
import HDWalletProvider from 'truffle-hdwallet-provider';
import * as config from './config.js';
import * as ipprotectJSON from './src/product.js';

const provider = new HDWalletProvider(env.config().parsed.mnemonic, config.default.matic.netURL);
const web3 = new Web3(provider);

const admin = env.config().parsed.adminAddress;
const abi = ipprotectJSON.default.abi;

http.createServer(function (req, res) {
    switch (req.url) {
        case '/address':
            if (req.method == 'GET') {
                var address = web3.eth.accounts.create();
                res.writeHead(200, {'Content-Type': 'application/json'})
                var result = JSON.stringify(address);
                res.write(result)
                res.end()
            }
            break;

        case '/getBrandAddress':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getBrandAddress().call();

                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getProductAddress':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getProductAddress().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getDateCreation':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getDateCreation().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getDateEvent':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getDateEvent().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;

        case '/getDetails':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getDetails().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getImages':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getImages().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;

        case '/getPurchasingPrice':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getPurchasingPrice().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getPaymentTerms':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getPaymentTerms().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;

        case '/getLeadTime':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getLeadTime().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getDeliveryTerms':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getDeliveryTerms().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;

        case '/getTermsAndConditions':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getTermsAndConditions().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;
        case '/getSold':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getSold().call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;


        case '/getRetailerBuy':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressContract = data.contract;
                    const addressRetailer = data.addressRetailer;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.getRetailerBuy(addressRetailer).call();
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;


        case '/contract':
            if (req.method == 'POST') {
                console.log(req);
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {

                    const contractToken = new web3.eth.Contract(ipprotectJSON.default.abi);
                    const byteCode = ipprotectJSON.default.bytecode;
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    console.log(data);
                    contractToken.deploy({
                        data: byteCode,
                        arguments: [
                            data.addressBrand,
                            data.addressProduct,
                            web3.utils.fromAscii(data.brandProductId),
                            data.systemProductId,
                            data.image1,
                            data.image2,
                            data.image3,
                            data.timeCreated,
                        ]
                    }).send({
                        from: admin
                        // gasPrice: 200000000000,
                        // gas: 5000000
                    })
                        .on('error', function (error) {
                            let result = {'error:': error};
                            res.writeHead(400, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                            console.log(result);
                        })
                        .then((instance) => {
                            let result = {'addresse': instance.options.address};
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        });
                });
            }
            break;
        case '/event':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const price = data.price;
                    const paymentTerms = data.paymentTerms;
                    const deliveryTerms = data.deliveryTerms;
                    const addressContract = data.addressContract;
                    const termsAndConditions = data.termsAndConditions;
                    const leadTime = data.leadTime;
                    const timelaynch = data.timelaunch;

                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {
                            let result = await IPProtect.methods.startEvent(
                                web3.utils.fromAscii(price),
                                web3.utils.fromAscii(paymentTerms),
                                web3.utils.fromAscii(leadTime),
                                web3.utils.fromAscii(deliveryTerms),
                                termsAndConditions,
                                timelaynch
                            )
                                .send({
                                    from: admin
                                });
                            console.log(result);
                            res.writeHead(200, {'Content-Type': 'application/json'})
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            console.log(e.stack);
                        }
                    })();
                })
                break;

            }
            break;

        case '/images':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    if (Array.isArray(body) && body.length == 0) {
                        res.writeHead(400, {'Content-Type': 'application/json'})
                        result = {
                            error: "Empty data"
                        }
                        res.end("");
                    }
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressProduct = data.addressProduct;

                    const image1 = data.image1;
                    const image2 = data.image2;
                    const image3 = data.image3;

                    const IPProtect = new web3.eth.Contract(abi, addressProduct);
                    (async () => {
                        try {

                            const response = await IPProtect.methods.setImages(image1, image2, image3).call();
                            let code = 200;
                            if (!response) {
                                code = 400;
                            }
                            res.writeHead(code, {'Content-Type': 'application/json'})
                            result = {
                                "success": response
                            };
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            res.writeHead(400, {'Content-Type': 'application/json'})
                            result = {
                                error: e.stack
                            }
                            res.end(JSON.stringify(result));
                        }
                    })();
                })
            }
            break;
        case '/retailer':
            if (req.method == 'POST') {
                let body = []
                req.on('data', chunk => {
                    body.push(chunk)
                })
                req.on('end', () => {
                    if (Array.isArray(body) && body.length == 0) {
                        res.writeHead(400, {'Content-Type': 'application/json'})
                        result = {
                            error: "Empty data"
                        }
                        res.end("");
                    }
                    let dataContract = body[0].toString('utf8');
                    let data = JSON.parse(dataContract);
                    const addressRetailer = data.retailerAddress;
                    const addressContract = data.contractAddress;
                    const amount = data.amount;
                    const IPProtect = new web3.eth.Contract(abi, addressContract);
                    (async () => {
                        try {

                            console.log('addressRetailer= ' + addressRetailer);
                            console.log('addressContract= ' + addressContract);
                            console.log('amount= ' + amount);
                            //const response =   IPProtect.methods.sell(addressRetailer, amount).call();
                            const response = await IPProtect.methods.sell(addressRetailer, amount)
                                .send({
                                    from: admin
                                });
                            let code = 200;
                            if (!response) {
                                code = 400;
                            }
                            res.writeHead(code, {'Content-Type': 'application/json'})
                            result = {
                                "success": response
                            };
                            res.end(JSON.stringify(result));
                        } catch (e) {
                            res.writeHead(400, {'Content-Type': 'application/json'})
                            result = {
                                error: e.stack
                            }
                            res.end(JSON.stringify(result));
                        }
                    })();
                })
            }
            break;
        default:
            res.writeHead(404, {'Content-Type': 'text/html'})
            res.end();
    }


}).listen(1337, '0.0.0.0')